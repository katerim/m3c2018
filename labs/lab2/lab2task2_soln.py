"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    X = np.zeros((Nt+1,M))

    for i in range(M):
        for j in range(Nt):
            X[j+1,i] = X[j,i] + np.sqrt(dt)*randn(1)

    Xm = np.mean(X,axis=1)
    Xv = np.var(X,axis=1)
    return X,Xm,Xv


def analyze(display=False):
    """Add code here to analyze the simlation error
    """

    Mvalues = [10,100,1000,10000]
    Nt=100
    Xvarray = np.zeros(len(Mvalues))

    for i,M in enumerate(Mvalues):
        _,_,Xv = brown1(Nt,M)
        print(i,M,Xv[-1])
        Xvarray[i] = Xv[-1]

    errorv = np.abs(Xvarray-Nt)

    if display:
        plt.figure()
        plt.loglog(Mvalues,errorv,'x--')

    return Mvalues,Xvarray,errorv
