!lab7 code, parallel matrix multiplication
!To compile: gfortran -fopenmp -o test.exe lab7.f90
!To run: ./test.exe


program lab7
    use omp_lib
    implicit none
    integer :: i1,j1,M,N,k1
    integer(kind=8) :: t1,t2,rate !timer variables
    real(kind=8) :: Csum, tmp
    real(kind=8), allocatable, dimension(:,:) :: A,B,C


    !read in problem parameters
    open(unit=10,file='data.in')
        read(10,*) M
        read(10,*) N
    close(10)


    !initialize variables
    allocate(A(M,N),B(N,M),C(M,M))


      call random_number(A)
      call random_number(B)

call system_clock(t1)

!Rewrite as loop and parallelize
!C = matmul(A,B)
!allocate(A(M,N),B(N,M),C(M,M))
!$OMP parallel do
do i1=1,M
    do j1=1,M
           C(i1,j1) = 0.0
           do k1=1,N
              C(i1,j1)  = C(i1,j1) + A(i1,k1) * B(k1,j1)
           enddo
        enddo
     enddo
!$OMP end parallel do

call system_clock(t2,rate)

Csum = sum(C)



print *, 'wall time:',float(t2-t1)/float(rate)
!test parallelized code:
print *, 'test C:',maxval(abs(C-matmul(A,B)))
print *, 'test Csum:',(Csum-sum(C))/Csum






end program lab7
