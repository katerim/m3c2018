! Lab 4, task 1

program task1
    implicit none
    integer :: i1
    integer, parameter :: N=5, display_iteration = 0
    real(kind=8) :: odd_sum

    call computeSum(N,odd_sum)

end program task1



subroutine computeSum(N, odd_sum, display_iteration)
  implicit none
  integer, intent(in) :: N, display_iteration
  real(kind=8), intent(out) :: odd_sum
  integer :: i1

  odd_sum = 0.d0
!Add a do-loop which sums the first N odd integers and prints the result
  do i1=1,N
    odd_sum = odd_sum + (2*i1) - 1.d0
    if (display_iteration == 1) then
      print *, ''
    end if
  end do
  print *,'odd sum=', odd_sum

end subroutine computeSum


! To compile this code:
! $ gfortran -o task1.exe lab41.f90
! To run the resulting executable: $ ./task1.exe
