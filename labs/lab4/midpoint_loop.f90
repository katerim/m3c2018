!midpoint.f90
!Compute the integral of 4/(1+x^2) from 0 to 1 using the midpoint rule
!Input: number of intervals, N
!Output (to screen): number of intervals, estimated value of integral, and error
!To compile this code: gfortran -o midpoint.exe midpoint.f90
!To run: ./midpoint.exe

program midpoint
	implicit none
  integer :: i1,N,Nlen,k,l
  real(kind=8) :: pi
  real(kind=8) :: dx, sum_i, xm, S, f, a, error
	real(kind=8), dimension(4) :: Nar, Sar, Ear


  Nar=(/1000,2000,4000,8000/)
	!print *, 'size Nar=', size(Nar)

  S = 0.d0 !initialize integral

  do k = 1,size(Nar)
		N = Nar(k)
		!print *, 'N=', N
    dx = 1.d0/dble(N) !interval size

    !loop over intervals computing each interval's contribution to integral
    do i1 = 1,N
        xm = dx*(dble(i1)-0.5d0) !midpoint of interval i1
        call integrand(xm,f)
        sum_i = dx*f
        S = S + sum_i !add contribution from interval to total integral

    end do
		Sar(k) = S
		!print *, 'Sum=', S
		!print *, 'Sumar=', Sar(k)
		S = 0.d0
	end do


  pi = asin(1.d0)*2.d0
	print *, 'proper pi=', pi

  do l = 1,size(Sar)
    Ear(l) = abs(Sar(l) - pi)
    print *, 'N=', Nar(l)
    !print *, 'sum=', Sar(l)
    !print *, 'error=', Ear(l)
		if (l>1) then
        print *, 'ratio=', (Ear(l)/Ear(l-1))
				!print *, (1/(Nar(l)*Nar(l)))
				print *, (Ear(l)/Nar(l)**(-2))

		end if
	end do

end program midpoint
!--------------------------------------------------------------------

!----------------------------------
!subroutine integrand
!   compute integrand, 4.0/(1+a^2)
!----------------------------------

subroutine integrand(a,f)
    implicit none
    real(kind=8), intent(in) :: a
    real(kind=8), intent(out) :: f
    f = 4.d0/(1.d0 + a*a)
end subroutine integrand
