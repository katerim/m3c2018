program loop
use omp_lib
implicit none

integer :: i1, threadID, numThreads
real (kind=8), dimension(12) :: x,y,z

call random_number(y)
call random_number(z)

!$OMP parallel do private(threadID)
do i1 = 1,12
  threadID = omp_get_thread_num()
  print *, 'iteration', i1, 'assigned to thread', threadID
  x(i1) = y(i1) + z(i1)
end do
!$OMP end parallel do

print *,'check:', maxval(abs(x-y-z))

end program loop
