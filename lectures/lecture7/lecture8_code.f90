program arrays
  implicit none
  integer :: i1
  integer, parameter :: c=2
  integer, dimension(4) :: x
  integer, dimension(5,4) :: A


  x=(/1,2,3,4/)

  do i1=1,size(A,1)
    A(i1,:) = i1*x
    print *,'i1,A(i1,:)=',i1,A(i1,:)
  end do

  end program arrays
