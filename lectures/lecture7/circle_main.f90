program circle_main

use circle_mod

implicit none
real(kind=8) :: radius,A


print *, 'pi=', circle_pi

call circle_area(radius,A)
print *,'area=', A


end program circle_main


! compile with gfortran -o circle.exe circle_mod.f90 circle_main.f90
