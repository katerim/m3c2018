module circle_mod
implicit none
real (kind=8), parameter :: pi = 2.d0*asin(1.d0)

contains

subroutine circle_area
implicit none
real (kind=8), intent(in) :: r
real (kind=8), intent(out) :: area

area = pi*r**2

end subroutine circle_area



end module circle_mod


!compile gfortran -c circle_mod.f90
